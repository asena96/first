#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QMainWindow>
#include <QLineF>
#include <cstdlib>
#include <QPointF>

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    virtual void paintEvent(QPaintEvent *event);
    void robot_step();
    void on_click();

private:
    Ui::MainWindow *ui;
    vector <QLineF> lines;
    float robot_x;
    float robot_y;
    float robot_fi, robot_fi_start;
    float robot_r;
    vector <QLineF> rob_lines;
    vector <QPointF> isections;
    vector <QPointF> isections_min;
};

#endif // MAINWINDOW_H



#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPainter>
#include <QPushButton>
#include <iostream>
#include <QLineF>
#include <QPointF>
#include <math.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Строим лабиринт
    int i;
    for(i=1; i<=8; i++)
        lines.push_back(QLineF(20*i, 20*i, 400-20*i, 20*i));

    for(i=9; i<=15; i++)
        lines.push_back(QLineF(340-20*i, 20*i, 80+20*i, 20*i));

    for(i=1; i<=7; i++)
        lines.push_back(QLineF(20*(i+1), 20*(i+1), 20*(i+1), 320-20*i));

    for(i=8; i<=14; i++)
        lines.push_back(QLineF(80+20*(i+1), 300-20*i, 80+20*(i+1), 20*(i+1)));

//    robot_x = 200 * (rand() + 0.0) / RAND_MAX;
//    robot_y = 200 * (rand() + 0.0) / RAND_MAX;
    robot_x = 150;
    robot_y = 150;
    robot_r = 4;
    cerr << robot_x << ' ' << robot_y << '\n';
    robot_fi_start = robot_fi = 30;
    rob_lines = lines;

    connect(ui->pushButton, &QPushButton::clicked,
            this, &MainWindow::on_click);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter p(this);

    //Рисуем лабиринт
    unsigned int i;
    for (i=0; i<lines.size(); i++)
        p.drawLine(lines[i]);

    //Рисуем робота
    p.setBrush(QBrush(Qt::green, Qt::SolidPattern));
    p.drawEllipse(robot_x - robot_r, robot_y - robot_r, 2*robot_r, 2*robot_r);

    for (i=0; i<rob_lines.size(); i++)
        p.drawLine(rob_lines[i]);
}


void MainWindow::on_click()
{
    robot_step();
    update(); // Вызвать paintEvent(происходит перерисовка сцены)
}

/*
 * @brief Шаг робота
 *
 * Посмотреть на окружающие стены и сделать шаг.
 */
void MainWindow::robot_step()
{
    rob_lines.resize(0);

    float dx, dy;
    //dy = robot_r * sin(robot_fi*3.14/180);
    //dx = robot_r * cos(robot_fi*3.14/180);

    dx = 200;

    // Выпускаем 8 лучей из робота
    unsigned int i;
    for (i=0; i<8; i++)
    {
        //cerr << robot_fi << ' ' << robot_fi_start << '\n';
        dy = dx * tan(robot_fi*3.14/180);

        if (robot_fi >= 0.0 && robot_fi < 90.0)
            rob_lines.push_back(QLineF(robot_x, robot_y, robot_x + dx, robot_y - dy)); // Дописать исключения!!!!!!!
        if (robot_fi>90 && robot_fi<=180)
            rob_lines.push_back(QLineF(robot_x, robot_y, robot_x - dx, robot_y + dy));
        if (robot_fi>180 && robot_fi<270)
            rob_lines.push_back(QLineF(robot_x, robot_y, robot_x - dx, robot_y + dy));
        if (robot_fi>270 && robot_fi<360)
            rob_lines.push_back(QLineF(robot_x, robot_y, robot_x + dx, robot_y - dy));
        robot_fi += (360/8);
     }
    robot_fi = robot_fi_start;

    // Смотрит на окружающие стены
    // Т.е. заполняем массив isections_min; это массив, в котором хранятся
    // данные(расстояние до ближайшей стены лабиринта) от каждого из 8 датчиков
    isections_min.resize(0);
    unsigned int j;
    for (j=0; j<rob_lines.size(); j++)
    {
        isections.resize(0);
        // Строим всевозможные пересечения лабиринта и j-го луча
        for (i=0; i<lines.size(); i++)
        {
            QPointF p;
            if (lines[i].intersect(rob_lines[j], &p) == QLineF::BoundedIntersection)
            {
                isections.push_back(p);
            }
        }
        /*
        for (i=0; i<isections.size(); i++)
        {
            cerr << isections[i].x() << ' ' << isections[i].y() << '\n';
        }
        */

        //Ищем ближайшее пересечение j-го луча от робота с лабиринтом
        //(т.е. min это расстояние до ближайшей стены для j-го луча)
        float min=10000;
        int index=-1;
        for (i=0; i<isections.size(); i++)
        {
            float z;
            z = sqrt(powf(robot_x - isections[i].x(), 2) + powf(robot_y - isections[i].y(), 2));
            if (min >= z)
            {
                min=z;
                index=i;
            }
            //cerr << z << '\n';
        }
        //cerr << "min=" << min << " index=" << index << "  x=" << isections[index].x() << " y=" << isections[index].y()<< '\n';

        QPointF c(isections[index].x(),isections[index].y());
        isections_min.push_back(c);
    }

    //cerr << isections_min.size() << '\n';
    float max = 0;
    int index = -1;
    for (i=0; i<isections_min.size(); i++)
    {
        cerr << isections_min[i].x() << ' ' << isections_min[i].y() << '\n';

        float z;
        z = sqrt(powf(robot_x - isections_min[i].x(), 2) + powf(robot_y - isections_min[i].y(), 2));
        if (max <= z)
        {
            max=z;
            index=i;
        }
    }
    cerr << max << ' ' << isections_min[index].x() << ' ' << isections_min[index].y() << '\n';

    robot_x = isections_min[index].x() + robot_r ; // ?? - или +
    robot_y = isections_min[index].y() + robot_r ; // ?? - или +

    //cerr << rob_lines.size() ;
    //cerr << robot_fi << '\n';
    //cerr << tan(30*3.14/180) << '\n' << sin(91*3.14/180)/cos(91*3.14/180) << '\n' << tan(120*3.14/180) << '\n' << tan(200*3.14/180) << '\n' << tan(340*3.14/180);

}


#include "mainwindow.h"
#include <QApplication>
#include <QLineF>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}